package lauriechanlam;

import java.util.*;

public class Triangle {
  public static String getTypeFromArgs(String[] args) {
    Triangle triangle = Triangle.createFromArgs(args);
    return triangle.getType();
  }

  public static Triangle createFromArgs(String[] args) {
    List<Integer> sortedEdges = createSortedEdgesFromArgs(args);
    return new Triangle(sortedEdges);
  }

  private static List<Integer> createSortedEdgesFromArgs(String[] args) {
    List<Integer> edges = new ArrayList<>();
    for (String arg : args) {
      final Integer edge;
      try {
        edge = Integer.parseInt(arg);
      } catch (IllegalArgumentException e) {
        throw new IllegalArgumentException(String.format("expected all edges to be positive integers, got %s", arg));
      }
      edges.add(edge);
    }
    Collections.sort(edges);
    return edges;
  }

  private Triangle(List<Integer> sortedEdges) {
    this.sortedEdges = sortedEdges;
    validate();
  }

  private void validate() {
    final String message = getInvalidityMessage();
    if (message != null) {
      throw new IllegalArgumentException(message);
    }
  }

  private String getInvalidityMessage() {
    if (sortedEdges.size() != 3) {
      return String.format("expected 3 arguments, got %d", sortedEdges.size());
    }
    if (sortedEdges.get(0) < 0) {
      return String.format("expected all edges to be positive integers, got %d", sortedEdges.get(0));
    }
    final boolean isTriangle = sortedEdges.get(0) + sortedEdges.get(1) >= sortedEdges.get(2);
    if (!isTriangle) {
      return String.format("expected the triangle inequality to be respected, got %d + %d < %d", sortedEdges.toArray());
    }
    return null;
  }

  public String getType() {
    Set<Integer> distinctEdges = new HashSet<>(sortedEdges);
    final int distinctEdgesCount = distinctEdges.size();
    return TYPE_PER_DISTINCT_EDGE_COUNT.get(distinctEdgesCount);
  }

  private static final Map<Integer, String> TYPE_PER_DISTINCT_EDGE_COUNT = new HashMap<Integer, String>() {{
    put(1, "equilateral");
    put(2, "isosceles");
    put(3, "scalene");
  }};

  final private List<Integer> sortedEdges;
}
