#!/bin/bash

mkdir -p bin/test/triangle/
javac -cp repository/*:bin/production/triangle test/lauriechanlam/TriangleTest.java -d bin/test/triangle/
java -jar repository/junit-platform-console-standalone-1.3.1.jar -cp repository/*:bin/production/triangle:bin/test/triangle/ -c lauriechanlam.TriangleTest
